SHELL = /bin/bash

schemas = $(sort $(patsubst src/%.yaml,schemas/%.json,$(wildcard src/*.yaml)))

define example_test
    @echo -n "Testing examples/$(1).yaml ... "
		@scripts/validate schemas/$(2).json examples/$(1).yaml
    @echo " done."
endef

.PHONY: clean test

all: convert

schemas/%.json: src/%.yaml
	@echo -n "Converting $< ..."
	@anymarkup convert -f yaml -t json $< > $@
	@echo " done."

convert: $(schemas)

test: convert
	@# This is quite fine, but if we do the pytest stuff mentioned bellow, we should convert this
	@# to pytest test case as well.
	@for schema in $(schemas); do \
		echo -n "Verifying $$schema ... " ; \
		scripts/validate schemas/jsonschema-draft-07.schema $$schema ; \
		echo " done." ; \
	done

	@# This is very temporary, just to get something that's using jsonschema to validate examples,
	@# to make clear we really want to have some examples and that we want to test our schemas with
	@# these examples.
	@#
	@# In the *close* future, we need more dense tests, without wasting one file for one test case;
	@# using pytest with parametrization would let us write a simple "verify given example" test,
	@# and feed it with dozen of test cases.
	$(call example_test,environment-variables,environment-variables)
	$(call example_test,compose-id,compose)
	$(call example_test,basic-environment,environment)

clean:
	rm -rf $(schemas)
