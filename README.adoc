= Eunomia

Temporary space for designing testing environment specification and related documents.

== Validation

[INFO]
====
For running validation script and schema tests, you need to install few Python packages. See
`requirements.txt` for the list. Feel free to install them as packaged for your distribution,
using your system package manager.
====


=== Schemas

To validate schemas and run their own tests, a `make` target is at hand:

[source,shell]
....
$ make test
....


=== Environment

To validate an environment specification, use `scripts/validate` tool:

[source,shell]
....
$ scripts/validate src/environment.yaml <your environment specification>
....

